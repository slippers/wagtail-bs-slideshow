from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock


class SlideshowDetailBlock(StructBlock):
    
    class Meta:
        template = 'wagtail_bs_slideshow/blocks/slideshow_detail_block.html'
        icon = 'media'
        help_text = _('Slideshow Detail Block')
