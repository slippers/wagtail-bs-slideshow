from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock, StreamBlock
from .slideshow_basic_block import SlideshowBasicBlock
from .slideshow_list_block import SlideshowListBlock
from .slideshow_nav_block import SlideshowNavBlock
from .slideshow_detail_block import SlideshowDetailBlock



class SlideshowBlocks(StreamBlock):
    slide_basic = SlideshowBasicBlock()
    slide_list = SlideshowListBlock()
    slide_nav = SlideshowNavBlock()
    slide_detail = SlideshowDetailBlock()

    class Meta:
        help_text = _('Slideshow Blocks')
