from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock


class SlideshowNavBlock(StructBlock):
    
    class Meta:
        template = 'wagtail_bs_slideshow/blocks/slideshow_nav_block.html'
        icon = 'media'
        help_text = _('Slideshow Navigation Block')
