from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock


class SlideshowListBlock(StructBlock):
    
    class Meta:
        template = 'wagtail_bs_slideshow/blocks/slideshow_list_block.html'
        icon = 'list'
        help_text = _('Slideshow List Block')
