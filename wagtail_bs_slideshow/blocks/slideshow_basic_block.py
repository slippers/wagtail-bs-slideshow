from django.utils.translation import gettext_lazy as _
from wagtail.blocks import StructBlock


class SlideshowBasicBlock(StructBlock):
    
    class Meta:
        template = 'wagtail_bs_slideshow/blocks/slideshow_basic_block.html'
        icon = 'media'
        help_text = _('Slideshow Basic Block')
