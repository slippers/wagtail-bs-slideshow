from django.apps import AppConfig


class WagtailBootstrapBaseConfig(AppConfig):
    name = 'wagtail_bs_slideshow'
    verbose_name = 'Wagtail Boostrap Slideshow'
    default_auto_field = 'django.db.models.AutoField'
