from django.db import models
from django.utils.translation import gettext_lazy as _
from django.shortcuts import get_object_or_404 
from django.db.models import Max
from wagtail.models import Page
from wagtail.search import index
from wagtail.fields import StreamField
from wagtail.admin.panels import (
        TabbedInterface,
        FieldPanel,
        InlinePanel,
        ObjectList
        )
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtailmenus.models import MenuPageMixin
from wagtailmenus.panels import menupage_panel
from wagtail_bs_blocks.blocks import get_layout_blocks
from ..blocks import SlideshowBlocks


class SlideshowPage(RoutablePageMixin, MenuPageMixin, Page):

    class Meta:
        verbose_name = _('Slideshow Bootstrap Page')

    # exposed in the template
    detail = None

    content = StreamField(
            get_layout_blocks(extra_blocks=[('slideshow_blocks', SlideshowBlocks()),]),
            blank=True,
            null=True,
            use_json_field=True)

    search_fields = Page.search_fields + [
            index.SearchField('get_searchable_contents'),
            ]

    content_panels = Page.content_panels + [
            FieldPanel('content'),
            ]

    settings_panels = Page.settings_panels + [
            menupage_panel
            ]

    edit_handler = TabbedInterface([
        ObjectList(content_panels, heading='Content'),
        ObjectList([InlinePanel('slideshow_details', label="Slideshow Details")], heading='Slideshow Details'),
        ObjectList(Page.promote_panels, heading='Promote'),
        ObjectList(settings_panels, heading='Settings', classname="settings"),
        ])

    @property
    def get_searchable_contents(self):
        text = []
        for element in self.slideshow_details.order_by('sort_order'):
            text.append(element.get_searchable_content)
        return ' '.join(text)

    @property
    def get_details(self):
        # django-modelculuster has limited operations on queryset while in preview mode
        # values_list returns a tuple object
        # details is FakeQuerySet
        details = self.slideshow_details.order_by('sort_order').values_list('title', 'uuid')
        details_dict = [{'title': item[0], 'uuid': item[1] } for item in details]
        for d in details_dict:
            d['active'] = d['uuid'] == self.detail.uuid
        return details_dict 

    @route(r'^$')
    def main(self, request):
        self.detail = self.slideshow_details.order_by('sort_order').first()
        return Page.serve(self, request)

    @route(r"^(?P<uuid>[-\w]+)/$")
    def detail_by_index(self, request, uuid):
        self.detail = get_object_or_404(self.slideshow_details, uuid=uuid)
        return Page.serve(self, request)

    @route(r"^(?P<uuid>[-\w]+)/next/$")
    def detail_next(self, request, uuid):
        current = get_object_or_404(self.slideshow_details, uuid=uuid)
        detail = self.slideshow_details.filter(sort_order=current.sort_order + 1)        
        if not detail:
            detail = self.slideshow_details.filter(sort_order=0)   
        self.detail = get_object_or_404(detail)
        return Page.serve(self, request)

    @route(r"^(?P<uuid>[-\w]+)/prev/$")
    def detail_previous(self, request, uuid):
        current = get_object_or_404(self.slideshow_details, uuid=uuid)
        detail = self.slideshow_details.filter(sort_order=current.sort_order - 1)        
        if not detail:
            max_sort_order = self.slideshow_details.aggregate(Max('sort_order'))['sort_order__max']
            detail = self.slideshow_details.filter(sort_order=max_sort_order)   
        self.detail = get_object_or_404(detail)
        return Page.serve(self, request)
