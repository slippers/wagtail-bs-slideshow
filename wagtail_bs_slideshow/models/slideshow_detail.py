import uuid
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.html import strip_tags
from wagtail.models import Orderable
from wagtail.fields import StreamField
from wagtail.admin.panels import (
    FieldPanel,
    FieldPanel
)
from wagtail_bs_blocks.blocks import CardBlocks
#django-modelcluster
from modelcluster.fields import ParentalKey
from .slideshow import SlideshowPage



'''new_uuid
create a new uuid for field default
without this a django migrations was being 
created each time.
'''
def new_uuid():
    return uuid.uuid4()


class SlideshowDetails(Orderable):

    class Meta:
        verbose_name = _('Slideshow Details')

    page = ParentalKey(SlideshowPage, on_delete=models.CASCADE, related_name='slideshow_details')

    uuid = models.UUIDField(default=new_uuid)
    
    title = models.CharField(max_length = 250)

    content = StreamField(CardBlocks(max_num=1, min_num=1, required=True), use_json_field=True)

    panels = [
        FieldPanel('title'),
        FieldPanel('content'),
    ]

    @property
    def get_searchable_content(self):
        return '{} {}'.format(
                self.title,
                strip_tags(
                    self.content.render_as_block()).replace('\n', ' ').strip()
                )
  
    def __str__(self):
       return self.title
