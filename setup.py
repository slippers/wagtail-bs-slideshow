from setuptools import setup


'''
https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files
https://docs.python.org/2/distutils/sourcedist.html#commands
'''
setup()
